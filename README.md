### README#

### How do I get set up? ###
certifi==2017.4.17
chardet==3.0.4
Django==1.11.2
django-cleanup==1.0.1
django-mptt==0.8.7
django-mptt-admin==0.4.6
django-mysql==2.1.0
idna==2.5
mysqlclient==1.3.10
olefile==0.44
Pillow==4.2.0
pytz==2017.2
requests==2.18.1
six==1.10.0
urllib3==1.21.1
