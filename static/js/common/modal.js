modal_module = {};

//блоки
modal_module.modal_window = $('.modal_window');
modal_module.page_content = $('.page_content');
modal_module.modal_close = $('.modal_close_button');
modal_module.text = $('.modal_text');

//функции
modal_module.show = modalShow;
modal_module.hide = modalHide;
modal_module.put = putText;
modal_module.append = append;

//*********************
//**события	
//*********************
modal_module.modal_close.on('click', function(){
	modalHide();
});
//закрыть окно если нажатие на escape
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
     	if (modal_module.modal_window.is(':visible')==true){
     		modal_module.hide();	
     	}
    }
});

//закрыть окно, если вне окна
$(document).click(function(event){
	if(!$(event.target).closest('.modal_content_inner').length){
		if($('.modal_content_inner').is(':visible')){
			modal_module.hide();
		}
	}
})

//*********************
//**функции
//*********************
function modalShow(){
	modal_module.modal_window.show(200);
	modal_module.page_content.hide(200);
	modal_module.modal_window.css({'display': 'flex'});
}

function modalHide(){
	modal_module.page_content.show(200);
	modal_module.modal_window.hide(200);
}

function putText(data){
	modal_module.text.html(data);
}

function append(data){
	modal_module.text.append(data);
}