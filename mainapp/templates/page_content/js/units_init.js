var units_module = {};

//элементы
units_module.units_htmls = $('.page_unit');

units_module.units = {{children|safe}};
units_module.cost_min = 10000;
units_module.cost_max = 0;

function Init(){
	//подтягиваем html
	$.each($('.page_unit'), function(i){
		html = $(this)
		id = parseInt(html.attr('data-id'));
		$.each(units_module.units, function(j){
			if(id== units_module.units[j]['id']){
				units_module.units[j]['html'] = html;
			}
		});
	});

	//подтягиваем минимальное - максимальное значение цены
	$.each(units_module.units, function(i){
		if (units_module.units[i]['cost'] > units_module.cost_max){
			units_module.cost_max = units_module.units[i]['cost'];
		}
		if (units_module.units[i]['cost'] < units_module.cost_min){
			units_module.cost_min = units_module.units[i]['cost'];
		}

	});

	if (typeof filter_module != 'undefined'){
		filter_module.parameters.cost['min_html'][0]['innerHTML'] = units_module.cost_min;
		filter_module.parameters.cost['max_html'][0]['innerHTML'] = units_module.cost_max;

		filter_module.parameters.cost['slider'][0]['dataset']['initialStart'] = units_module.cost_min;
		filter_module.parameters.cost['slider'][0]['dataset']['initialEnd'] = units_module.cost_max;
	}
}

Init();