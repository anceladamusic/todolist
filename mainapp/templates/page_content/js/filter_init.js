var filter_module = {};

//переменные 
filter_module.parameters = {};
filter_module.parameters.cost = {};

//блоки


filter_module.parameters.cost['min_html'] = $('.filter_range_min');
filter_module.parameters.cost['max_html'] = $('.filter_range_max');
filter_module.parameters.cost['slider'] = $('.slider');
filter_module.parameters.min_html = $('.minimum');
filter_module.parameters.max_html = $('.maximum');

filter_module.parameters.min_value = false;
filter_module.parameters.max_value = false;
//функции


function filter_init(){
}


//события

//изменение минимума
filter_module.parameters.min_html.attrchange({
    trackValues: true, /* Default to false, if set to true the event object is 
                updated with old and new value.*/
    callback: function (event) { 
    	value = filter_module.parameters.min_html.attr('aria-valuenow');

        filter_module.parameters.min_value = parseInt(value);

    	filter_module.parameters.cost['min_html'][0]['innerHTML'] = parseInt(value);

        $.each(units_module.units, function(i){
            if (units_module.units[i]['cost'] < value){
                units_module.units[i]['html'].hide(200);
            } else {
                units_module.units[i]['html'].show(200);
            }
        });

        //event               - event object
        //event.attributeName - Name of the attribute modified
        //event.oldValue      - Previous value of the modified attribute
        //event.newValue      - New value of the modified attribute
        //Triggered when the selected elements attribute is added/updated/removed
    }        
});

//изменение максимума
filter_module.parameters.max_html.attrchange({
    trackValues: true, /* Default to false, if set to true the event object is 
                updated with old and new value.*/
    callback: function (event) {
    	value = filter_module.parameters.max_html.attr('aria-valuenow');

        filter_module.parameters.max_value = parseInt(value);

    	filter_module.parameters.cost['max_html'][0]['innerHTML'] = parseInt(value);

        $.each(units_module.units, function(i){
            if (units_module.units[i]['cost'] > value){
                units_module.units[i]['html'].hide(200);
            } else {
                units_module.units[i]['html'].show(200);
            }
        });

        //event               - event object
        //event.attributeName - Name of the attribute modified
        //event.oldValue      - Previous value of the modified attribute
        //event.newValue      - New value of the modified attribute
        //Triggered when the selected elements attribute is added/updated/removed
    }        
});