import json

from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.contenttypes.models import ContentType
from django.apps import apps

from .models import *
from .base import Base

from .sms import SMS
from .mail import Mail

# from oauth.properties import Properties

def make_ajax(request):

	if request.method == 'POST':

		string = json.loads(request.body.decode('utf-8'))

		# получить форму заказа
		if string['method'] == 'get_order_form':

			result = ''

			return JsonResponse({
				'result': result,	
			})