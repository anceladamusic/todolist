from django.conf.urls import url
from . import views
from . import ajax

urlpatterns = [
    url(r'^$', views.mainpage),
    url(r'^ajax/$', ajax.make_ajax, name='ajax'),
]