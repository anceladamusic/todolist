from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from .page_interpreter import PageInterpreter
from .base import Base

def mainpage(request):

	page = PageInterpreter().build_mainpage(request)

	content = page['html']

	topbar = Base().build_topbar(request)
	bottombar = Base().build_bottombar()

	return render(request, 'main.html', {
		'top': {
			'topbar': topbar,
		},
		'content': content,
		'bottombar': bottombar,
	})